package com.danielsmanioto.customersapi.adapters.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/customers")
@RestController
public class CustomerController {

    @GetMapping
    public ResponseEntity<?> findAllCustomers() {
        return ResponseEntity.ok("OK");
    }

}
